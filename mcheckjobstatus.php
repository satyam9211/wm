<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body background="waste4.jpg">

<img src="waste.jpg" alt="Waste Management">
<p align="center" style="margin-top: 1em">
<?php


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "persondetails";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT reportid, contractorid, jobstatus FROM jobdone";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Report ID</th><th>Contractor ID</th><th>Job Status</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" .$row["reportid"]. "</td><td>" . $row["contractorid"]. "</td><td>" . $row["jobstatus"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "Oops... No Record Found!";
}
$conn->close();
?>
</p>
</body>
</html>
