<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body background="waste4.jpg">
<img src="waste.jpg" alt="Waste Management">

<p align="center" style="margin-top: 1em">

<?php


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "persondetails";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT reportid, waste, location FROM reportgarbage";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>ReportID</th><th>Type of Waste</th><th>Location</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" .$row["reportid"]. "</td><td>" . $row["waste"]. "</td><td>" . $row["location"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}
$conn->close();
?>
</p>
</body>
</html>
