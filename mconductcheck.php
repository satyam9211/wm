<!DOCTYPE html>
<html>
<head>
<title>conduct check</title>
</head>
<body background="waste4.jpg">
<img src="waste.jpg" alt="Waste Management">

<p align="center" style="margin-top: 1em">
<?php


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "persondetails";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT  jobdone.reportid, jobdone.contractorid, userfeedback.id, userfeedback.feedback, userfeedback.comment FROM  jobdone,
          userfeedback WHERE jobdone.reportid=userfeedback.id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table border=1px><tr><th>Report ID</th><th>Contractor ID</th><th>Job Satisfaction</th><th>Comment</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" .$row["id"]. "</td><td>" . $row["contractorid"]. "</td><td>" . $row["feedback"]. "</td><td>".
		$row["comment"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}
$conn->close();
?>
</p>
</body>
</html>
